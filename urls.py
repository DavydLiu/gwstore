from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView
from django.contrib import admin
admin.autodiscover()
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from gwstore.core.views import get_user, get_product, get_order, submit_order


urlpatterns = patterns("",
   url(r'^$', TemplateView.as_view(template_name='homepage.html'), name='home'),
   url(r"^product/(?P<product_id>\d+)$", get_product),
   url(r"^user/(?P<user_id>\d+)$", get_user),
   url(r"^order/(?P<user_id>\d+)$", get_order),
   url(r'^place-order$', submit_order),
   url(r"^admin/", include(admin.site.urls))
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns += staticfiles_urlpatterns()