$(document).ready(function() {

    // Function after popup display
//    function afterFadeIn(loginBox) {
//        if (loginBox == 'order-box') {
//            // Update item list and calculate total cost
//            $('#check-out').click() {
//                var products = $(:checked).getParent().id;
//                alert(products);
//            }
//        }
//    }

    // Display popup
    $('#nav-bar-content').on('click', 'a.popup', function() {
        // Getting the variable's value from a link
        var loginBox = $(this).attr('href');
        if (loginBox != '#') {
            // Fade in the Popup
            $(loginBox).fadeIn(300);

            // Set the center alignment padding + border see css style
            var popMargTop = ($(loginBox).height() + 24) / 2;
            var popMargLeft = ($(loginBox).width() + 24) / 2;

            $(loginBox).css({
                'margin-top' : -popMargTop,
                'margin-left' : -popMargLeft
            });

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);
        }
        return false;
    });

    // When clicking on the button close or the mask layer the popup closed
    function closePopup() {
        $('#mask , .popup-form').fadeOut(300 , function() {
            $('#mask').remove();
        });
        return false;
    }
    $('a.close, #mask').live('click', function() {
        closePopup();
    });

    // Error messaging
    function errorMessage(parent, message) {
        parent.find('[name="error"]').text(message);
        if (message == '') {
            parent.find(':button').removeAttr('disabled');
        } else {
            parent.find(':button').attr('disabled', 'disabled');
        }
        return false;
    }

    // Get parent
    function getParent(item) {
        var parent = item.parents('div');
        return parent;
    }

    // Check if user entered necessary information
    $('[name="email"], [name="name"], [name="address"], [name="city"], [name="state"], [name="zipcode"]').blur(function() {
        $('[name="email"], [name="name"], [name="address"], [name="city"], [name="state"], [name="zipcode"]').each(function() {
            var parent = getParent($(this));
            var labelName = $(this).prev().html();
            if ($(this).val() == '') {
                var errorMes = 'Please enter the ' + labelName;
                errorMessage(parent, errorMes);
                return false;
            } else {
                if (labelName == 'Email') {
                    function isValidEmailAddress(email) {
                        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
                        return pattern.test(email);
                    }
                    if (!isValidEmailAddress($(this).val())) {
                        errorMessage(parent, 'Please enter a valid email address.');
                        return false;
                    }
                } else {
                    errorMessage(parent, '');
                }
            }
        });
    });

    // Update item list and calculate total cost
    $('#check-out').click( function() {
        // Initialize form
        $('#total-price').html('0.00');
        $("[name='products']").attr('value', '');
        $('#items').children().remove();
        $('#placeorder').attr('disabled', 'disabled');
        $('.buy').filter(':checked').closest('div').each(function(index) {
            // Total price
            var totalPrice = (parseFloat($('#total-price').html()) + parseFloat($(this).children('.price').html())).toFixed(2);
            $('#total-price').html(totalPrice);
            // Products
            if (index == 0) {
                var products = $(this).attr('id');
            } else {
                var products = $("[name='products']").attr('value') + ',' + $(this).attr('id');
            }
            $("[name='products']").attr('value', products);
            // Items
            $('#items').append('<li>' + $(this).children('.title').html() + ' $' + $(this).children('.price').html() + '</li>');
            // Button
            if (totalPrice > 0) {
                $('#placeorder').attr('disabled', '');
            } else {
                $('#placeorder').attr('disabled', 'disabled');
            }
        });
        // Tax
        $("[name='state']").change(function() {
            var totalPrice = parseFloat($('#total-price').html());
            var state = $(this).attr('value');
            var taxRate = 0.00;
            switch(state) {
                case 'Alabama':
                    taxRate = 0.04;
                    break;
                case 'Alaska':
                    taxRate = 0.00;
                    break;
                case 'Arizona':
                    taxRate = 0.06;
                    break;
                case 'Arkansas':
                    taxRate = 0.06;
                    break;
                case 'California':
                    taxRate = 0.07;
                    break;
                case 'Colorado':
                    taxRate = 0.03;
                    break;
                case 'Connecticut':
                    taxRate = 0.06;
                    break;
                case 'Delaware':
                    taxRate = 0.00;
                    break;
                case 'District of Columbia':
                    taxRate = 0.06;
                    break;
                case 'Florida':
                    taxRate = 0.06;
                    break;
                case 'Georgia':
                    taxRate = 0.04;
                    break;
                case 'Hawaii':
                    taxRate = 0.04;
                    break;
                case 'Idaho':
                    taxRate = 0.06;
                    break;
                case 'Illinois':
                    taxRate = 0.06;
                    break;
                case 'Indiana':
                    taxRate = 0.07;
                    break;
                case 'Iowa':
                    taxRate = 0.06;
                    break;
                case 'Kansas':
                    taxRate = 0.06;
                    break;
                case 'Kentucky':
                    taxRate = 0.06;
                    break;
                case 'Louisiana':
                    taxRate = 0.04;
                    break;
                case 'Maine':
                    taxRate = 0.05;
                    break;
                case 'Maryland':
                    taxRate = 0.06;
                    break;
                case 'Massachusetts':
                    taxRate = 0.06;
                    break;
                case 'Michigan':
                    taxRate = 0.06;
                    break;
                case 'Minnesota':
                    taxRate = 0.07;
                    break;
                case 'Mississippi':
                    taxRate = 0.07;
                    break;
                case 'Missouri':
                    taxRate = 0.04;
                    break;
                case 'Montana':
                    taxRate = 0.00;
                    break;
                case 'Nebraska':
                    taxRate = 0.05;
                    break;
                case 'Nevada':
                    taxRate = 0.07;
                    break;
                case 'New Hampshire':
                    taxRate = 0.00;
                    break;
                case 'New Jersey':
                    taxRate = 0.07;
                    break;
                case 'New Mexico':
                    taxRate = 0.05;
                    break;
                case 'New York':
                    taxRate = 0.04;
                    break;
                case 'North Carolina':
                    taxRate = 0.05;
                    break;
                case 'North Dakota':
                    taxRate = 0.05;
                    break;
                case 'Ohio':
                    taxRate = 0.06;
                    break;
                case 'Oklahoma':
                    taxRate = 0.05;
                    break;
                case 'Oregon':
                    taxRate = 0.00;
                    break;
                case 'Pennsylvania':
                    taxRate = 0.06;
                    break;
                case 'Rhode Island':
                    taxRate = 0.07;
                    break;
                case 'South Carolina':
                    taxRate = 0.06;
                    break;
                case 'South Dakota':
                    taxRate = 0.04;
                    break;
                case 'Tennessee':
                    taxRate = 0.07;
                    break;
                case 'Texas':
                    taxRate = 0.06;
                    break;
                case 'Utah':
                    taxRate = 0.05;
                    break;
                case 'Vermont':
                    taxRate = 0.06;
                    break;
                case 'Virginia':
                    taxRate = 0.04;
                    break;
                case 'Washington':
                    taxRate = 0.06;
                    break;
                case 'West Virginia':
                    taxRate = 0.06;
                    break;
                case 'Wisconsin':
                    taxRate = 0.05;
                    break;
                case 'Wyoming':
                    taxRate = 0.04;
                    break;
            }
            $('#tax').html((totalPrice * taxRate).toFixed(2));
            var tax = parseFloat($('#tax').html());
            $("[name='tax']").attr('value', tax.toFixed(2));
            $("[name='totalcost']").attr('value', (totalPrice + tax).toFixed(2));
        });
    })
    $('#placeorder').click( function() {
        if ($(this).attr('disabled') !== 'disabled') {
            $.post('/place-order', $('#order-form').serialize(), function(data) {
//                    alert(data);
//                if (data == 1) {
                    alert('Thanks, your order has been placed!');
//                }
            })
        }
    })

});