$(document).ready(function() {
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
    }
    
    $('#header-menu').on('click', 'a.popup', function() {
        // Getting the variable's value from a link 
        var loginBox = $(this).attr('href');
        if (loginBox != '#') {
            // Fade in the Popup
            $(loginBox).fadeIn(300);
            
            // Set the center alignment padding + border see css style
            var popMargTop = ($(loginBox).height() + 24) / 2; 
            var popMargLeft = ($(loginBox).width() + 24) / 2; 
            
            $(loginBox).css({ 
                'margin-top' : -popMargTop,
                'margin-left' : -popMargLeft
            });
            
            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);
        }
        return false;
    });
    
    // When clicking on the button close or the mask layer the popup closed
    function closePopup() {
        $('#mask , .popup-form').fadeOut(300 , function() {
            $('#mask').remove();  
        }); 
        return false;   
    }
    $('a.close, #mask').live('click', function() { 
        closePopup();
    });
    
    // Error messaging
    function errorMessage(parent, message) {
        parent.find('[name="error"]').text(message);
        if (message == '') {
            parent.find(':button').removeAttr('disabled');
        }
        return false;
    }
    
    // Get parent
    function getParent(item) {
        var parent = item.parents('div');
        return parent;
    }
    
    // Check if user entered username and username is available
    $('#login-box, #register-box').find('[name="username"]').blur(function() {
        var parent = getParent($(this));
        if ($(this).val() == '') {
            errorMessage(parent, 'Please enter the username.');
        } else {
            errorMessage(parent, '');
        }
        if (parent.attr('id') == 'register-box') {
            $.post('./include/check_availability.php', {check: 'username', username: $(this).val()}, function(data) {
                if (data == 1) {
                    errorMessage(parent, '');
                } else if (data == 0) {
                    errorMessage(parent, 'Username is not available.');
                }
            });
        }
    });
    
    //Check if user entered valid email address
    $('#register-box, #user-panel').find('[name="email"]').blur(function() {
        var parent = getParent($(this));
        function isValidEmailAddress(email) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(email);
        }
        
        var email = $(this).val();
        if (!isValidEmailAddress(email)) {
            errorMessage(parent, 'Please enter a valid email address.');
        } else {
            $.post('./include/check_availability.php', {check: 'email', 'email': email}, function(data) {
                // alert(data);
                if (data == 1) {
                    errorMessage(parent, '');
                } else if (data == 0) {
                    errorMessage(parent, 'Email address is not available.');
                }
            });
            errorMessage(parent, '');
        }
    });
    
    // Check if user entered password
    $('#login-box, #register-box').find('[name="password"], [name="oldpassword"]').blur(function() {
        var parent = getParent($(this));
        // alert(parent);
        if ($(this).val() == '') errorMessage(parent, 'Please enter the password.');
        else {
            errorMessage(parent, '');
        }
    });
    $('#user-panel').find('[name="oldpassword"]').blur(function() {
        var parent = getParent($(this));
        // alert(parent);
        if ($(this).val() == '') errorMessage(parent, 'Please enter the password.');
        else {
            errorMessage(parent, '');
        }
    });
    
    // Check if the passwords match
    $('#register-box, #user-panel').find('[name="retypepassword"]').blur(function() {
       var parent = getParent($(this));
        if ($(this).val() !== parent.find('[name="password"]').val()) {
            errorMessage(parent, 'The passwords do not match.');
        } else {
            errorMessage(parent, '');
        }
    });
    
    // Hash password
    function passHash (element) {
        if (element.val() != '') {
            element.val($.md5(element.val()));
        }
        return false;
    }
    
    // login
    function login(data) {
        var html = '<a id="logout" href="#" class="popup">Logout</a><a href="#" class="popup">Last Login: ' + data.login_time + '<a href="#user-panel" class="popup">' + data.username + '</a>';
        $('#header-menu').html(html);
        // put information in the user panel.
        $('#user-panel').find('[name="username"]').val(data.username);
        $('#user-panel').find('[name="firstname"]').val(data.first_name);
        $('#user-panel').find('[name="lastname"]').val(data.last_name);
        $('#user-panel').find('[name="email"]').val(data.email);
        var html = '<button id="search-button" button">Search</button><input id="search-input" text" name="company" /><p id="search-info"></p><input id="search-username"value="' + data.username + 'name="username" style="display:none;" />';
        $('#header-subbar').html(html);
        $('[name="password"]').val('');
        showGraph();
        return false;
    }
    
    //Sign user in
    $('#sign-in').click(function() {
        var parent = getParent($(this));
        passHash(parent.find('[name="password"]'));
        $.post('./include/login.php', $('#login-form').serialize(), function(data) {
            // alert(data);
            var data = JSON.parse(data);
            switch(data.status) {
                case 1:
                    $('#login-form').find('input').val('');
                    closePopup();
                    login(data);
                    break;
                case 0:
                    errorMessage(parent, 'Username or password is wrong.');
                    break;
                case 2:
                    errorMessage(parent, 'User is not found.');
                    break;
            }
        });
    });
    
    //Register user
    $('#register').click(function() {
        var parent = getParent($(this));
        passHash(parent.find('[name="password"]'));
        passHash(parent.find('[name="retypepassword"]'));
        $.post('./include/register.php', $('#register-form').serialize(), function(data) {
            var data = JSON.parse(data);
            if (data.status == 1) {
                login(data);
                parent.find('input').val('');
                closePopup();
            }    
        });
    });
    
    // Update user information
    $('#update').click(function() {
        var parent = getParent($(this));
        passHash(parent.find('[name="oldpassword"]'));
        passHash(parent.find('[name="password"]'));
        passHash(parent.find('[name="retypepassword"]'));
        $.post('./include/update.php', $('#update-form').serialize(), function(data) {
            // alert(data);
            if (data == 1) closePopup();
            else if (data == 0) errorMessage(parent, 'Old password is wrong.');
            else if (data == 2) errorMessage(parent, 'Database is down, please update later.');
        });
        parent.find('[name="oldpassword"], [name="password"], [name="retypepassword"]').val('');
    });
    
    //Logout
    $('#header-menu').on('click', '#logout', function() {
        $.post('./include/logout.php', function(data) {
            if (data == 1) {
                var html = '<a href="#register-box" class="popup">Register</a><a href="#login-box" class="popup">Login</a>';
                $('#header-menu').html(html);
                 $('#header-subbar').html('');
                showGraph();
            }
        });
    });

});