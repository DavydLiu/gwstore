from django import template
from gwstore.core.models import Product

register = template.Library()


@register.assignment_tag
def products_tag():
    return Product.objects.order_by("?")
