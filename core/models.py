from django.db import models
from sorl.thumbnail import ImageField

class User(models.Model):
    first_name = first_name = models.CharField(max_length=125)
    last_name = models.CharField(max_length=125)
    email = models.EmailField()
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(auto_now_add=True)

class Product(models.Model):
    name = models.CharField(max_length=255)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    description = models.TextField()
    photo = ImageField(upload_to="product_photos")

class Order(models.Model):
    # user = models.ForeignKey(User)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    products = models.CommaSeparatedIntegerField(max_length=255)
    total_cost = models.DecimalField(max_digits=7, decimal_places=2)
    tax = models.DecimalField(max_digits=5, decimal_places=2)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=50)
    zipcode = models.IntegerField()
    time = models.DateTimeField(auto_now=True)