from django.shortcuts import get_object_or_404
from django.utils import simplejson
from django.http import HttpResponse
from decimal import Decimal
import datetime

from gwstore.core.models import Product, User, Order


def get_product(request, product_id):
    if request.is_ajax():
        product = get_object_or_404(Product, id=product_id)
        message = simplejson.dumps(product)
        return HttpResponse(message, mimetype="application/json")

def get_user(request, user_id):
    if request.is_ajax():
        user = get_object_or_404(User, id=user_id)
        message = simplejson.dumps(user)
        return HttpResponse(message, mimetype="application/json")

def get_order(request, user_id):
    if request.is_ajax():
        order = get_object_or_404(Order, user=user_id)
        message = simplejson.dumps(order)
        return HttpResponse(message, mimetype="application/json")

def submit_order(request):
    if request.is_ajax():
        order = Order(email = request.POST.get('email'),
                      name = request.POST.get('name'),
                      address = request.POST.get('address'),
                      city = request.POST.get('city'),
                      state = request.POST.get('state'),
                      zipcode = request.POST.get('zipcode'),
                      tax = Decimal(request.POST.get('tax')),
                      products = request.POST.get('products'),
                      total_cost = Decimal(request.POST.get('totalcost'))
                )
        if order.save():
            return HttpResponse(1)
        else:
            return HttpResponse(0)