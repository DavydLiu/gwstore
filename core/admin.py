from django.contrib import admin
from gwstore.core.models import Product, User

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'quantity')

class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'last_login')

admin.site.register(Product, ProductAdmin)
admin.site.register(User, UserAdmin)